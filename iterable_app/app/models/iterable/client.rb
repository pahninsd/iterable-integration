require 'json'
require 'net/http'

module Iterable
  class Client

    def get(endpoint, params = {})
      http, request = build_get_request(endpoint, params)

      response = http.request(request)
      handle_response(response)
    end

    def post(endpoint, body)
      http, request = build_post_request(endpoint, body)

      response = http.request(request)
      handle_response(response)
    end

    def reset_data
      get('/data/reset')
    end

    def all_data
      get('/data/view')
    end

    private

    def build_post_request(endpoint, body)
      url = URI.parse(base_url + endpoint)

      http = Net::HTTP.new(url.host, url.port)
      request = Net::HTTP::Post.new(url.path, { 'Content-Type' => 'application/json' })
      request.body = JSON.generate(body)

      [http, request]
    end

    def build_get_request(endpoint, query_params = {})
      url = URI.parse(base_url + endpoint + '?' + URI.encode_www_form(query_params))

      http = Net::HTTP.new(url.host, url.port)
      request = Net::HTTP::Get.new(url.request_uri, { 'Content-Type' => 'application/json' })

      [http, request]
    end

    def base_url      
      Rails.env.production? ? "https://api.iterable.com" : "http://iterable-mock-api:4567"
      #"http://iterable-mock-api:4567"
    end

    def handle_response(response)
      parsed_response = JSON.parse(response.body)

      if response.code.to_i == 200 && parsed_response['code'] == 'Success'
        parsed_response
      else
        { 'code' => 'Failed', 'error' => 'Request failed', 'response_code' => response.code, 'response_body' => response.body }
      end
    rescue JSON::ParserError
      { 'code' => 'Failed', 'error' => 'Invalid JSON in response', 'response_body' => response.body }
    end
  end
end
