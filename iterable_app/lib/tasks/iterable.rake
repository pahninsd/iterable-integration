# lib/tasks/iterable.rake

namespace :iterable do
  task create_campaign: :environment do
    config_file_path = Rails.root.join('config', 'iterable.yml')

    unless File.exist?(config_file_path)
      File.open(config_file_path, 'w') { |file| file.write("campaign_id: \n") }
    end

    iterable_config = YAML.load_file(config_file_path)
    campaign_id = iterable_config['campaign_id']

    unless campaign_id
      # Create Template
      Iterable::Template.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      
      # Create List
      default_list = Iterable::List.create(name: 'Event B', description: 'To maintain list of users clicked on Event B')
      list_id = default_list.id

      # Get Template ID
      template_id = Iterable::Template.find_by_client_template_id(client_template_id: 'test_template_id')["templates"][0]["templateId"]

      # Create Campaign
      campaign = Iterable::Campaign.create(
        name: 'Rails APP Integration',
        list_ids: [list_id],
        template_id: template_id,
        send_at: '2024-01-14',
        send_mode: 'ProjectTimeZone',
        start_time_zone: 'UTC',
        default_time_zone: 'UTC',
        data_fields: { key: 'value' }
      )

      # Update config file with campaign_id
      campaign_id = campaign["campaignId"]
      iterable_config['campaign_id'] = campaign_id
      iterable_config['list_id'] = list_id
      File.open(config_file_path, 'w') { |file| file.write(iterable_config.to_yaml) }
      
      puts "Campaign created with ID: #{campaign_id}"
    else
      puts "Campaign ID already exists in config file: #{campaign_id}"
    end
  end
end
