module Iterable
  class Event
    def self.track(email: nil, user_id: nil, event_name:, id: nil, created_at: nil, data_fields: {}, campaign_id: 0, template_id: 0, create_new_fields: true)
      body = {
        email: email,
        userId: user_id,
        eventName: event_name,
        id: id,
        createdAt: created_at,
        dataFields: data_fields,
        campaignId: campaign_id,
        templateId: template_id,
        createNewFields: create_new_fields
      }

      Client.new.post('/api/events/track', body)
    end
  end
end
