require 'rails_helper'

RSpec.describe Iterable::List, type: :model do
  before { Iterable::Client.new.reset_data }

  describe '.all' do
    it 'returns lists successfully' do
      response = described_class.all
      expect(response['code']).to eq('Success')
    end
  end

  describe '.create' do
    context 'with valid parameters' do
      it 'creates a list successfully' do
        response = described_class.create(name: 'Example List 1', description: 'List description')
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid parameters' do
      it 'handles bad JSON body' do
        response = described_class.create(name: nil, description: 'List description')
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end

  describe '.subscribers' do
    context 'with valid parameters' do
      it 'returns users from a list successfully' do
        list = described_class.create(name: 'Example List', description: 'List description')
        user_creation  = Iterable::User.sync(email: 'user94@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
        subscribe_response = list.subscribe_user(email: 'user94@example.com')
        response = list.subscribers
        expect(response['code']).to eq('Success')
        expect(response['users']).to be_an(Array)
        expect(response['users'].size).to eq(1)
        expect(response['users'][0]['email']).to eq('user94@example.com')
      end
    end

    context 'with invalid parameters' do
      it 'handles list not found' do
        response = described_class.new(id: 999).subscribers
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end

  describe '.subscribe_user' do
    context 'with valid parameters' do
      it 'subscribes users to a list successfully' do
        list = described_class.create(name: 'Example List', description: 'List description')
        user_creation  = Iterable::User.sync(email: 'user@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
        response = list.subscribe_user(email: 'user@example.com' )
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid parameters' do
      it 'fails with user is not found in iterable API' do
        list = described_class.create(name: 'Example List', description: 'List description')
        response = list.subscribe_user(email: 'user@example.com' )
        expect(response['code']).to eq('Success')
        expect(response['failCount']).to eq(1)
      end
      
      it 'handles list not found' do
        response = described_class.new(id: 999).subscribe_user(email: 'example@example.com')
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end

      xit 'handles bad JSON body' do
        list = described_class.create(name: 'Example List', description: 'List description')
        response = list.subscribe_user(email: nil)
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end
end
