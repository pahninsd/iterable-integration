require 'sinatra'
require 'json'

class Application < Sinatra::Base
  lists_data = {}
  users_data = {}
  templates_data = {}
  campaigns_data = {}

  configure do
    enable :logging
  end

  # Debugging endpoint 1 
  get '/data/reset' do
    lists_data = {}
    users_data = {}
    templates_data = {}
    campaigns_data = {}

    response_data = {
      "msg" => "string",
      "code" => "Success",
      "params" => {}
    }
    content_type :json
    JSON.generate(response_data)
  end

  # Debugging endpoint 2
  get '/data/view' do
    response_data = {
      "lists" => lists_data,
      "users" => users_data,
      "templates" => templates_data,
      "campaigns" => campaigns_data
    }

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 1: /api/events/track
  post '/api/events/track' do
    # Sample payload for reference
    # {
    #   "email": "string",
    #   "userId": "string",
    #   "eventName": "string",
    #   "id": "string",
    #   "createdAt": 0,
    #   "dataFields": {},
    #   "campaignId": 0,
    #   "templateId": 0,
    #   "createNewFields": true
    # }

    request_body = JSON.parse(request.body.read)

    if request_body['eventName']
      response_data = {
        "msg" => "string",
        "code" => "Success",
        "params" => {}
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => {}
      }
    end

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 2: /api/lists
  get '/api/lists' do
    # Return the current lists data (empty by default)
    content_type :json
    response_data = {
      "msg" => "string",
      "code" => "Success",
      "lists" => lists_data
    }
    JSON.generate(response_data)
  end

  # Endpoint 3: /api/lists
  post '/api/lists' do
    # Sample payload for reference
    # {
    #   "name": "string",
    #   "description": "string"
    # }
    request_body = JSON.parse(request.body.read)

    if request_body['name']
      # Store the list data in memory
      list_id = lists_data.length + 1
      lists_data[list_id] = {
        "id" => list_id,
        "name" => request_body['name'],
        "description" => request_body['description'],
        "createdAt" => Time.now.to_i,
        "listType" => "Standard",
        "subscribers" => [] # Initialize subscribers array
      }

      response_data = {
        "msg" => "string",
        "code" => "Success",
        "listId" => list_id
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => {}
      }
    end

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 4: /api/lists/subscribe
  post '/api/lists/subscribe' do
    # Sample payload for reference
    # {
    #   "listId": 0,
    #   "subscribers": [
    #     {
    #       "email": "string",
    #       "dataFields": {},
    #       "userId": "string",
    #       "preferUserId": true,
    #       "mergeNestedObjects": true
    #     }
    #   ],
    #   "updateExistingUsersOnly": true
    # }
    request_body = JSON.parse(request.body.read)

    if request_body['subscribers']
      list_id = request_body['listId']

      # Throw error if listId is not present in the stored list
      unless lists_data.key?(list_id)
        halt 400, JSON.generate({
          "msg" => "List not found",
          "code" => "ListNotFound",
          "params" => request_body
        })
      end

      # Perform logic to subscribe users to the list
      success_count = 0
      fail_count = 0

      request_body['subscribers'].each do |subscriber|
        user_key = subscriber['email'] || subscriber['userId']
        if users_data.key?(user_key)
          lists_data[list_id]['subscribers'] << subscriber
          success_count += 1
        else
          fail_count += 1
        end
      end

      response_data = {
        "msg" => "string",
        "code" => "Success",
        "successCount" => success_count,
        "failCount" => fail_count,
        "invalidEmails" => [],
        "invalidUserIds" => [],
        "filteredOutFields" => [],
        "createdFields" => []
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => {}
      }
    end

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 5: /api/lists/getUsers
  get '/api/lists/getUsers' do
    list_id = params['listId'].to_i

    unless lists_data.key?(list_id)
      halt 400,
           JSON.generate(
             {
               "msg" => "List not found",
               "code" => "ListNotFound",
               "params" => params
             }
           )
    end
    # Return users data for the specified list (empty by default)
    list_users_data = lists_data[list_id]['subscribers'] || []

    content_type :json
    response_data = {
      "msg" => "string",
      "code" => "Success",
      "users" => list_users_data
    }

    JSON.generate(response_data)
  end

  # Endpoint 6: /api/users/update
  post '/api/users/update' do
    # Sample payload for reference
    # {
    #   "email": "string",
    #   "userId": "string",
    #   "dataFields": {},
    #   "preferUserId": true,
    #   "mergeNestedObjects": true,
    #   "createNewFields": true
    # }
    request_body = JSON.parse(request.body.read)

    if request_body['preferUserId'] || request_body['email']
      user_key = request_body['preferUserId'] ? request_body['userId'] : request_body['email']

      # Check if the user is present in users_data hash
      if users_data.key?(user_key)
        # Update dataFields if the user is present
        users_data[user_key]['dataFields'].merge!(request_body['dataFields']) if request_body['mergeNestedObjects']
        users_data[user_key]['dataFields'].merge!(request_body['dataFields']) if request_body['createNewFields']
      else
        # Add a new user if not present
        users_data[user_key] = {
          'email' => request_body['email'],
          'userId' => request_body['userId'],
          'dataFields' => request_body['dataFields']
        }
      end

      response_data = {
        "msg" => "string",
        "code" => "Success",
        "params" => users_data[user_key]
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => {}
      }
    end

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 7: /api/templates/email/upsert
  post '/api/templates/email/upsert' do
    # Sample payload reference
    # {
    #   "clientTemplateId": "string",
    #   "name": "string",
    #   "fromName": "string",
    #   "fromEmail": "string",
    #   "replyToEmail": "string",
    #   "subject": "string",
    #   "preheaderText": "string",
    #   "ccEmails": [
    #     "string"
    #   ],
    #   "bccEmails": [
    #     "string"
    #   ],
    #   "html": "string",
    #   "plainText": "string",
    #   "googleAnalyticsCampaignName": "string",
    #   "linkParams": [
    #     {
    #       "key": "string",
    #       "value": "string"
    #     }
    #   ],
    #   "dataFeedId": 0,
    #   "dataFeedIds": [
    #     0
    #   ],
    #   "cacheDataFeed": {},
    #   "mergeDataFeedContext": true,
    #   "locale": "string",
    #   "messageTypeId": 0,
    #   "creatorUserId": "string",
    #   "isDefaultLocale": true,
    #   "messageMedium": {}
    # }
    request_body = JSON.parse(request.body.read)

    if request_body['clientTemplateId']
      client_template_id = request_body['clientTemplateId']
      templates_data[client_template_id] ||= {}
      templates_data[client_template_id].merge!(request_body)
      templates_data[client_template_id]['templateId'] ||= templates_data[client_template_id].size

      response_data = {
        "msg" => "string",
        "code" => "Success",
        "params" => templates_data[client_template_id]
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => request_body
      }
    end

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 8: /api/templates/getByClientTemplateId
  get '/api/templates/getByClientTemplateId' do
    client_template_id = params['clientTemplateId']

    template_data = templates_data[client_template_id]

    response_data = {
      "msg" => "string",
      "code" => "Success",
      "templates" => [template_data].compact
    }

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 9: /api/campaigns/create
  post '/api/campaigns/create' do
    # Sample payload for referance
    # {
    #   "name": "string",
    #   "listIds": [
    #     0
    #   ],
    #   "templateId": 0,
    #   "suppressionListIds": [
    #     0
    #   ],
    #   "sendAt": "string",
    #   "sendMode": "ProjectTimeZone",
    #   "startTimeZone": "string",
    #   "defaultTimeZone": "string",
    #   "dataFields": {}
    # }
    request_body = JSON.parse(request.body.read)

    if request_body['listIds'] && request_body['templateId']
      # Validate listIds against lists_data
      invalid_list_ids = request_body['listIds'].reject { |list_id| lists_data.key?(list_id) }
      unless invalid_list_ids.empty?
        halt 400, JSON.generate({
          "msg" => "List not found",
          "code" => "ListNotFound",
          "params" => {}
        })
      end

      # Validate templateId against templates_data
      template_id = request_body['templateId']
      template = templates_data.find { |_, v| v['templateId'] == template_id }&.last
      if template.nil?
        halt 400, JSON.generate({
          "msg" => "Template not found",
          "code" => "TemplateNotFound",
          "params" => {}
        })
      end

      # Store data in campaigns_data
      campaign_id = campaigns_data.length + 1
      campaigns_data[campaign_id] = {
        "name" => request_body['name'],
        "listIds" => request_body['listIds'],
        "templateId" => template_id,
        "suppressionListIds" => request_body['suppressionListIds'],
        "sendAt" => request_body['sendAt'],
        "sendMode" => request_body['sendMode'],
        "startTimeZone" => request_body['startTimeZone'],
        "defaultTimeZone" => request_body['defaultTimeZone'],
        "dataFields" => request_body['dataFields']
      }

      response_data = {
        "msg" => "string",
        "code" => "Success",
        "campaignId" => campaign_id
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => request_body
      }
    end

    content_type :json
    JSON.generate(response_data)
  end

  # Endpoint 10: /api/email/target
  post '/api/email/target' do
    # Sample payload for referance
    # {
    #   "campaignId": 0,
    #   "recipientEmail": "string",
    #   "recipientUserId": "string",
    #   "dataFields": {},
    #   "sendAt": "string",
    #   "allowRepeatMarketingSends": true,
    #   "metadata": {}
    # }
    request_body = JSON.parse(request.body.read)

    if request_body['campaignId']
      campaign_id = request_body['campaignId']

      # Validate campaignId against campaigns_data
      unless campaigns_data.key?(campaign_id)
        halt 400, JSON.generate({
          "msg" => "Campaign not found",
          "code" => "CampaignNotFound",
          "params" => request_body
        })
      end

      user_key = request_body['recipientEmail'] || request_body['recipientUserId']
      unless users_data.key?(user_key)
        halt 400, JSON.generate({
          "msg" => "User not found",
          "code" => "Failure",
          "params" => request_body
        })
      end

      # send email

      response_data = {
        "msg" => "string",
        "code" => "Success",
        "params" => {}
      }
    else
      response_data = {
        "msg" => "string",
        "code" => "BadJsonBody",
        "params" => {}
      }
    end

    content_type :json
    JSON.generate(response_data)
  end
end
