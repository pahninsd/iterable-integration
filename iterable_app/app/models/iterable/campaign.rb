module Iterable
  class Campaign
    def self.create(name:, list_ids:, template_id:, suppression_list_ids: [], send_at: nil, send_mode: nil, start_time_zone: nil, default_time_zone: nil, data_fields: {})
      Client.new.post(
        '/api/campaigns/create',
        {
          name: name,
          listIds: list_ids,
          templateId: template_id,
          suppressionListIds: suppression_list_ids,
          sendAt: send_at,
          sendMode: send_mode,
          startTimeZone: start_time_zone,
          defaultTimeZone: default_time_zone,
          dataFields: data_fields
        }
      )
    end
  end
end
