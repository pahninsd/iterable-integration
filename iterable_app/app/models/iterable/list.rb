module Iterable
  class List
    attr_accessor :id

    def initialize(id: , response: {})
      @id = id
      @response = response
    end

    def [](key)
      @response[key]
    end

    def self.all
      Client.new.get('/api/lists')
    end

    def self.create(name:, description:)
      response = Client.new.post('/api/lists', { name: name, description: description })
      new(id: response['listId'], response: response)
    end

    def subscribe_user(email: )
      response = subscribers # get current list
      return response unless response["code"] == "Success"

      array_of_subs = response["users"]

      unless array_of_subs.any? { |x| x["email"] == email } # check if we need to add
        array_of_subs << { email: email }
        Client.new.post(
          '/api/lists/subscribe',
          {
            listId: @id,
            subscribers: array_of_subs,
            updateExistingUsersOnly: true
          }
        )
      end
    end

    def subscribers
      Client.new.get('/api/lists/getUsers', { listId: @id })
    end
  end
end
