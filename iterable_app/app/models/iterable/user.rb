module Iterable
  class User
    def self.sync(email: nil, user_id: nil, data_fields: {}, prefer_user_id: true, merge_nested_objects: true, create_new_fields: true)
      Client.new.post(
        '/api/users/update',
        {
          email: email,
          userId: user_id,
          dataFields: data_fields,
          preferUserId: prefer_user_id,
          mergeNestedObjects: merge_nested_objects,
          createNewFields: create_new_fields
        }
      )
    end
  end
end
