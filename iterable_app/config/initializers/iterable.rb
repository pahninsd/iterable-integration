if File.exist?(Rails.root.join('config', 'iterable.yml'))
  iterable_config = YAML.load_file(Rails.root.join('config', 'iterable.yml'))
  DEFAULT_CAMPAIGN = iterable_config['campaign_id']
  DEFAULT_LIST = iterable_config['list_id']
end
