FROM ruby

WORKDIR /home/app

ENV PORT 3000

EXPOSE $PORT
RUN apt-get update -qq
RUN gem install bundler
WORKDIR /home/app/iterable_app
COPY iterable_app/Gemfile*  .
RUN bundle install
COPY iterable_app /home/app/iterable_app

ENTRYPOINT [ "/bin/bash", "docker-entry.sh" ]
