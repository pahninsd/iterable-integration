module Iterable
  class Template

    def self.upsert_email_template(client_template_id:, name:, from_name:, from_email:, reply_to_email:, subject:, preheader_text:, cc_emails: [], bcc_emails: [], html:, plain_text:, google_analytics_campaign_name: nil, link_params: [], data_feed_id: 0, data_feed_ids: [], cache_data_feed: {}, merge_data_feed_context: true, locale: '', message_type_id: 0, creator_user_id: '', is_default_locale: true, message_medium: {})
      Client.new.post(
        '/api/templates/email/upsert',
        {
          clientTemplateId: client_template_id,
          name: name,
          fromName: from_name,
          fromEmail: from_email,
          replyToEmail: reply_to_email,
          subject: subject,
          preheaderText: preheader_text,
          ccEmails: cc_emails,
          bccEmails: bcc_emails,
          html: html,
          plainText: plain_text,
          googleAnalyticsCampaignName: google_analytics_campaign_name,
          linkParams: link_params,
          dataFeedId: data_feed_id,
          dataFeedIds: data_feed_ids,
          cacheDataFeed: cache_data_feed,
          mergeDataFeedContext: merge_data_feed_context,
          locale: locale,
          messageTypeId: message_type_id,
          creatorUserId: creator_user_id,
          isDefaultLocale: is_default_locale,
          messageMedium: message_medium
        }
      )
    end

    def self.find_by_client_template_id(client_template_id: )
      Client.new.get('/api/templates/getByClientTemplateId', { clientTemplateId: client_template_id })
    end
  end
end
