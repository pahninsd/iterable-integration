require 'rails_helper'

RSpec.describe Iterable::Event, type: :model do
  before { Iterable::Client.new.reset_data }

  describe '.track' do
    context 'with valid parameters' do
      it 'tracks an event successfully' do
        response = described_class.track(event_name: 'example_event')
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid parameters' do
      it 'handles bad JSON body' do
        response = described_class.track(event_name: nil)
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end
end
