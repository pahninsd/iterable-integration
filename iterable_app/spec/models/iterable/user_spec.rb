require 'rails_helper'

RSpec.describe Iterable::User, type: :model do
  before { Iterable::Client.new.reset_data }
  describe '.sync' do
    context 'with valid parameters' do
      it 'updates user data successfully' do
        response = described_class.sync(email: 'user@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
        expect(response['code']).to eq('Success')
      end
    end
  end
end
