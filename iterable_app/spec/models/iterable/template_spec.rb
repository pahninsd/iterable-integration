require 'rails_helper'

RSpec.describe Iterable::Template, type: :model do
  before { Iterable::Client.new.reset_data }

  describe '.upsert_email_template' do
    context 'with valid parameters' do
      it 'creates or updates an email template successfully' do
        response = described_class.upsert_email_template(
          client_template_id: 'test_template_id',
          name: 'Updated Template',
          from_name: 'Jane Doe',
          from_email: 'jane@example.com',
          reply_to_email: 'reply@example.com',
          subject: 'Updated Subject',
          preheader_text: 'Updated Preheader',
          html: '<html><body>Updated HTML</body></html>',
          plain_text: 'Updated Plain Text'
        )
        expect(JSON.parse(Iterable::Client.new.all_data["response_body"])["templates"]["test_template_id"]["name"]).to eq('Updated Template')
        expect(response['code']).to eq('Success')
      end
    end
  end


  describe '.find_by_client_template_id' do
    let(:client_template_id) {
      upsert = described_class.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      return JSON.parse(Iterable::Client.new.all_data["response_body"])["templates"]["test_template_id"]["clientTemplateId"]
    }
    context 'with valid parameters' do
      it 'returns a template by client template ID successfully' do
        response = described_class.find_by_client_template_id(client_template_id: client_template_id)
        expect(response['code']).to eq('Success')
        expect(response['templates']).to be_an(Array)
        expect(response['templates'].size).to eq(1)
        expect(response['templates'][0]['name']).to eq('Updated Template')
      end
    end

    context 'with invalid parameters' do
      it 'handles template not found' do
        response = described_class.find_by_client_template_id(client_template_id: 'invalid_template_id')
        expect(response['code']).to eq('Success')
        expect(response['templates']).to eq([])
      end
    end
  end
end
