require_relative '../lib/iterable_client'

RSpec.describe IterableClient do
  let(:list_id) { nil }

  before { described_class.reset_data }

  describe '.track_event' do
    context 'with valid parameters' do
      it 'tracks an event successfully' do
        response = described_class.track_event(event_name: 'example_event')
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid parameters' do
      it 'handles bad JSON body' do
        response = described_class.track_event(event_name: nil)
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end

  describe '.get_lists' do
    it 'returns lists successfully' do
      response = described_class.get_lists
      expect(response['code']).to eq('Success')
    end
  end

  describe '.create_list' do
    context 'with valid parameters' do
      it 'creates a list successfully' do
        response = described_class.create_list(name: 'Example List 1', description: 'List description')
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid parameters' do
      it 'handles bad JSON body' do
        response = described_class.create_list(name: nil, description: 'List description')
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end

  describe '.subscribe_to_list' do
    context 'with valid parameters' do
      it 'subscribes users to a list successfully' do
        list = described_class.create_list(name: 'Example List', description: 'List description')
        user_creation  = described_class.update_user(email: 'user@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
        response = described_class.subscribe_to_list(list_id: list['listId'], subscribers: [{ 'email' => 'user@example.com' }])
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid parameters' do
      it 'fails with user is not found' do
        list = described_class.create_list(name: 'Example List', description: 'List description')
        response = described_class.subscribe_to_list(list_id: list['listId'], subscribers: [{ 'email' => 'user234@example.com' }])
        expect(response['code']).to eq('Success')
        expect(response['failCount']).to eq(1)
      end
      
      it 'handles list not found' do
        response = described_class.subscribe_to_list(list_id: 999, subscribers: [{ 'email' => 'example@example.com' }])
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end

      it 'handles bad JSON body' do
        response = described_class.subscribe_to_list(list_id: 1, subscribers: nil)
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end

  describe '.update_user' do
    context 'with valid parameters' do
      it 'updates user data successfully' do
        response = described_class.update_user(email: 'user@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
        expect(response['code']).to eq('Success')
      end
    end
  end

  describe '.get_users_from_list' do
    context 'with valid parameters' do
      it 'returns users from a list successfully' do
        list = described_class.create_list(name: 'Example List', description: 'List description')
        user_creation  = described_class.update_user(email: 'user94@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
        subscribe_response = described_class.subscribe_to_list(list_id: list['listId'], subscribers: [{ 'email' => 'user94@example.com' }])
        response = described_class.get_users_from_list(list_id: list['listId'])
        expect(response['code']).to eq('Success')
        expect(response['users']).to be_an(Array)
        expect(response['users'].size).to eq(1)
        expect(response['users'][0]['email']).to eq('user94@example.com')
      end
    end

    context 'with invalid parameters' do
      it 'handles list not found' do
        response = described_class.get_users_from_list(list_id: 999)
        expect(response['code']).to eq('Failed')
        expect(response['error']).to eq('Request failed')
      end
    end
  end

  describe '.upsert_email_template' do
    context 'with valid parameters' do
      it 'creates or updates an email template successfully' do
        response = described_class.upsert_email_template(
          client_template_id: 'test_template_id',
          name: 'Updated Template',
          from_name: 'Jane Doe',
          from_email: 'jane@example.com',
          reply_to_email: 'reply@example.com',
          subject: 'Updated Subject',
          preheader_text: 'Updated Preheader',
          html: '<html><body>Updated HTML</body></html>',
          plain_text: 'Updated Plain Text'
        )
        expect(JSON.parse(described_class.get_data["response_body"])["templates"]["test_template_id"]["name"]).to eq('Updated Template')
        expect(response['code']).to eq('Success')
      end
    end
  end


  describe '.get_template_by_client_template_id' do
    let(:template_id) {
      upsert = described_class.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      return JSON.parse(described_class.get_data["response_body"])["templates"]["test_template_id"]["clientTemplateId"]
    }
    context 'with valid parameters' do
      it 'returns a template by client template ID successfully' do
        response = described_class.get_template_by_client_template_id(client_template_id: template_id)
        expect(response['code']).to eq('Success')
        expect(response['templates']).to be_an(Array)
        expect(response['templates'].size).to eq(1)
        expect(response['templates'][0]['name']).to eq('Updated Template')
      end
    end

    context 'with invalid parameters' do
      it 'handles template not found' do
        response = described_class.get_template_by_client_template_id(client_template_id: 'invalid_template_id')
        expect(response['code']).to eq('Success')
        expect(response['templates']).to eq([])
      end
    end
  end

  describe '.create_campaign' do
    let(:template_id) {
      upsert = described_class.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      return JSON.parse(described_class.get_data["response_body"])["templates"]["test_template_id"]["clientTemplateId"]
    }

    let(:list1) { described_class.create_list(name: 'Example List', description: 'List description')["listId"] }
    let(:list2) { described_class.create_list(name: 'Example List', description: 'List description')["listId"] }

    context 'with valid parameters' do
      it 'sends a POST request to the correct endpoint with the correct parameters' do
        response = described_class.create_campaign(
          name: 'Campaign Name',
          list_ids: [list1, list2],
          template_id: template_id,
          send_at: '2024-01-14',
          send_mode: 'ProjectTimeZone',
          start_time_zone: 'UTC',
          default_time_zone: 'UTC',
          data_fields: { key: 'value' }
        )
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid JSON body' do
      it 'returns an error response' do
        response = described_class.create_campaign(
          name: 'Campaign Name',
          list_ids: [],
          template_id: nil
        )

        expect(response['code']).to eq('Failed')
      end
    end

    context 'with invalid listId or templateId' do
      it 'returns an error response if listId is not found' do
        response = described_class.create_campaign(
          name: 'Campaign Name',
          list_ids: [99],
          template_id: template_id
        )

        expect(response['code']).to eq('Failed')
      end

      it 'returns an error response if templateId is not found' do
        response = described_class.create_campaign(
          name: 'Campaign Name',
          list_ids: [list1, list2],
          template_id: 99
        )

        expect(response['code']).to eq('Failed')
      end
    end
  end

  describe '.target_email' do
    let(:template_id) {
      upsert = described_class.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      return JSON.parse(described_class.get_data["response_body"])["templates"]["test_template_id"]["clientTemplateId"]
    }

    let(:list1) { described_class.create_list(name: 'Example List', description: 'List description')["listId"] }
    let(:list2) { described_class.create_list(name: 'Example List', description: 'List description')["listId"] }
    
    let!(:valid_campaign_id) {
      rt = described_class.create_campaign(
        name: 'Campaign Name',
        list_ids: [list1, list2],
        template_id: template_id,
        send_at: '2024-01-14',
        send_mode: 'ProjectTimeZone',
        start_time_zone: 'UTC',
        default_time_zone: 'UTC',
        data_fields: { key: 'value' }
      )
      return rt["campaignId"]
    }
    let!(:valid_recipient_email) {
      described_class.update_user(email: 'user@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
      'user@example.com'
    }

    context 'with valid input' do
      it 'sends a request and returns success response' do
        response = described_class.target_email(campaign_id: valid_campaign_id, recipient_email: valid_recipient_email)
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid campaignId' do
      it 'returns error response' do
        response = described_class.target_email(campaign_id: nil, recipient_email: valid_recipient_email)
        expect(response['code']).to eq('Failed')
      end
    end

    context 'with invalid user key' do
      it 'returns error response' do
        response = described_class.target_email(campaign_id: valid_campaign_id, recipient_email: 'invalid@example.com')
        expect(response['code']).to eq('Failed')
      end
    end

    context 'with invalid JSON body' do
      it 'returns error response' do
        response = described_class.target_email(campaign_id: nil)
        expect(response['code']).to eq('Failed')
      end
    end
  end
end
