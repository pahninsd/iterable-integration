require 'rails_helper'

RSpec.describe Iterable::Email, type: :model do
  before { Iterable::Client.new.reset_data }

  describe '.target' do
    let(:template_id) {
      Iterable::Template.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      return Iterable::Template.find_by_client_template_id(client_template_id: 'test_template_id')["templates"][0]["templateId"]
    }

    let(:list1) { Iterable::List.create(name: 'Example List', description: 'List description') }
    let(:list2) { Iterable::List.create(name: 'Example List', description: 'List description') }    
    let!(:valid_campaign_id) {
      campaign = Iterable::Campaign.create(
        name: 'Campaign Name',
        list_ids: [list1.id, list2.id],
        template_id: template_id,
        send_at: '2024-01-14',
        send_mode: 'ProjectTimeZone',
        start_time_zone: 'UTC',
        default_time_zone: 'UTC',
        data_fields: { key: 'value' }
      )

      return campaign["campaignId"]
    }
    let!(:valid_recipient_email) {
      Iterable::User.sync(email: 'user@example.com', prefer_user_id: false, data_fields: { 'age' => 25 })
      'user@example.com'
    }

    context 'with valid input' do
      it 'sends a request and returns success response' do
        response = described_class.target(campaign_id: valid_campaign_id, recipient_email: valid_recipient_email)
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid campaignId' do
      it 'returns error response' do
        response = described_class.target(campaign_id: nil, recipient_email: valid_recipient_email)
        expect(response['code']).to eq('Failed')
      end
    end

    context 'with invalid user key' do
      it 'returns error response' do
        response = described_class.target(campaign_id: valid_campaign_id, recipient_email: 'invalid@example.com')
        expect(response['code']).to eq('Failed')
      end
    end

    context 'with invalid JSON body' do
      it 'returns error response' do
        response = described_class.target(campaign_id: nil)
        expect(response['code']).to eq('Failed')
      end
    end
  end
end
