require 'rails_helper'

RSpec.describe "HomeControllers", type: :request do
  include Devise::Test::IntegrationHelpers

  let(:user) { User.create!(email: "test@example.com", password: "password123" ) } 
  before { sign_in user }
  describe 'GET #index' do
    it 'returns http success' do
      get "/"
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #event_a' do
    it 'tracks Event A and redirects to root path' do
      expect(Iterable::Event).to receive(:track).with(email: user.email, event_name: 'Event A')
      get "/event_a"
      expect(response).to redirect_to(root_path)
    end
  end

  describe 'GET #event_b' do
    it 'tracks Event B, adds user to list, sends emails, and redirects to root path' do
      expect(Iterable::Event).to receive(:track).with(email: user.email, event_name: 'Event B')
      # expect_any_instance_of(Iterable::List).to receive(:subscribe_user).with(email: user.email).and_return({"users" => []})
      
      get "/event_b"
      expect(response).to redirect_to(root_path)
    end

    it "triggers send email API" do
      Iterable::User.sync(email: user.email, prefer_user_id: false)
      expect(Iterable::Email).to receive(:target).with(recipient_email: user.email, campaign_id: DEFAULT_CAMPAIGN)
      get "/event_b"
    end
  end
end
