FROM ruby

WORKDIR /home/app

ENV PORT 4567

EXPOSE $PORT
RUN apt-get update -qq
RUN gem install bundler
WORKDIR /home/app/mock-server
COPY Gemfile*  /home/app/mock-server
RUN bundle install
COPY . /home/app/mock-server

CMD [ "puma", "-C", "puma.rb" ]
