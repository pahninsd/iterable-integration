require 'rails_helper'

RSpec.describe Iterable::Campaign, type: :model do
  before { Iterable::Client.new.reset_data }

  describe '.create_campaign' do
    let(:template_id) {
      Iterable::Template.upsert_email_template(
        client_template_id: 'test_template_id',
        name: 'Updated Template',
        from_name: 'Jane Doe',
        from_email: 'jane@example.com',
        reply_to_email: 'reply@example.com',
        subject: 'Updated Subject',
        preheader_text: 'Updated Preheader',
        html: '<html><body>Updated HTML</body></html>',
        plain_text: 'Updated Plain Text'
      )
      return Iterable::Template.find_by_client_template_id(client_template_id: 'test_template_id')["templates"][0]["templateId"]
    }

    let(:list1) { Iterable::List.create(name: 'Example List', description: 'List description') }
    let(:list2) { Iterable::List.create(name: 'Example List', description: 'List description') }

    context 'with valid parameters' do
      it 'sends a POST request to the correct endpoint with the correct parameters' do
        response = described_class.create(
          name: 'Campaign Name',
          list_ids: [list1.id, list2.id],
          template_id: template_id,
          send_at: '2024-01-14',
          send_mode: 'ProjectTimeZone',
          start_time_zone: 'UTC',
          default_time_zone: 'UTC',
          data_fields: { key: 'value' }
        )
        expect(response['code']).to eq('Success')
      end
    end

    context 'with invalid JSON body' do
      it 'returns an error response' do
        response = described_class.create(
          name: 'Campaign Name',
          list_ids: [],
          template_id: nil
        )

        expect(response['code']).to eq('Failed')
      end
    end

    context 'with invalid listId or templateId' do
      it 'returns an error response if listId is not found' do
        response = described_class.create(
          name: 'Campaign Name',
          list_ids: [99],
          template_id: template_id
        )

        expect(response['code']).to eq('Failed')
      end

      it 'returns an error response if templateId is not found' do
        response = described_class.create(
          name: 'Campaign Name',
          list_ids: [list1.id, list2.id],
          template_id: 99
        )

        expect(response['code']).to eq('Failed')
      end
    end
  end
end
