class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def event_a
    track_event("Event A")
    redirect_to root_path
  end

  def event_b
    track_event("Event B")
    add_user_to_list
    send_email_to_all_event_b_users
    redirect_to root_path
  end

  private

  def track_event(name)
    Iterable::Event.track(email: current_user.email, event_name: name)
  end

  def default_list
    @list ||= Iterable::List.new(id: DEFAULT_LIST)
  end

  def add_user_to_list
    default_list.subscribe_user(email: current_user.email)
  end

  def send_email_to_all_event_b_users
    default_list.subscribers["users"].each { |sub|
      Iterable::Email.target(recipient_email: sub["email"], campaign_id: DEFAULT_LIST)
    }
  end
end
