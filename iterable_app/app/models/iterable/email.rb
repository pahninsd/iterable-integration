module Iterable
  class Email
    def self.target(campaign_id:, recipient_email: nil, recipient_user_id: nil, data_fields: {}, send_at: nil, allow_repeat_marketing_sends: true, metadata: {})
      Client.new.post(
        '/api/email/target',
        {
          campaignId: campaign_id,
          recipientEmail: recipient_email,
          recipientUserId: recipient_user_id,
          dataFields: data_fields,
          sendAt: send_at,
          allowRepeatMarketingSends: allow_repeat_marketing_sends,
          metadata: metadata
        }
      )
    end
  end
end
